﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace webfashion.Models
{
    public class Product
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public double Price { get; set; }
        public string Thumbnail { get; set; }
    }
}